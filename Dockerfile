# BUILD
FROM debian:bullseye-slim AS builder

LABEL maintainer="greg.lebreton@hotmail.com"

RUN apt-get update && apt-get install -y \
    ca-certificates \
    unzip \
    sed \
    p7zip-full \
    coffeescript \
    xz-utils \
    wget \
    --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

# https://github.com/libretro/RetroArch/tree/master/pkg/emscripten
# https://buildbot.libretro.com/stable/
ENV RETROARCH_VERSION="1.19.0"
ENV ROOT_WWW_PATH="/var/www/html"

RUN mkdir -p ${ROOT_WWW_PATH} \
    && cd ${ROOT_WWW_PATH} \
    && wget https://buildbot.libretro.com/stable/${RETROARCH_VERSION}/emscripten/RetroArch.7z \
    && 7z x -y RetroArch.7z \
    && mv retroarch/* . \
    && rmdir retroarch \
    && sed -i '/<script src="analytics.js"><\/script>/d' ./index.html \
    && chmod +x indexer \
    && mkdir -p ${ROOT_WWW_PATH}/assets/cores \
    && cd ${ROOT_WWW_PATH}/assets/frontend \
    && cat bundle.zip.* > bundle.zip \
    && unzip bundle.zip \
    && cd bundle \
    && ../../../indexer > .index-xhr \
    && cd ${ROOT_WWW_PATH}/assets/cores \
    && ../../indexer > .index-xhr \
    && rm -rf ${ROOT_WWW_PATH}/RetroArch.7z \
    && rm -rf ${ROOT_WWW_PATH}/assets/frontend/bundle.zip

# FINAL 
FROM debian:bullseye-slim

LABEL maintainer="greg.lebreton@hotmail.com"

RUN apt-get update && apt-get install -y \
    nginx \
    --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

ENV ROOT_WWW_PATH="/var/www/html"

COPY --from=builder ${ROOT_WWW_PATH} ${ROOT_WWW_PATH}
COPY ./index.html ${ROOT_WWW_PATH}

WORKDIR ${ROOT_WWW_PATH}

EXPOSE 80

COPY entrypoint.sh /

CMD ["sh", "/entrypoint.sh"]
