# Dockerfile pour déployer un conteneur "retroarchwebplayer" 

Retroarch dans un navigateur web

## USAGE

### USE PRE BUILT IMAGE

```bash
docker run -d -p 8080:80 greglebreton/retroarchweb:1.5
```

### BUILD IMAGE

```bash
docker build -t retroarchweb .
docker run -d -p 8080:80 retroarchweb:latest
```

## COMPATIBILITE

- Compatible Linux AMD64 / ARM64 (raspberry pi 64 bits only)
